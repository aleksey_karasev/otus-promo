﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Entityframework
{
    public static class EntityFrameworkInstaller
    {
        public static IServiceCollection ConfigureContext(this IServiceCollection services,
            string connectionString)
        {
            services.AddDbContext<ApplicationDbContext>(optionsBuilder
                => optionsBuilder
                .UseLazyLoadingProxies()
                .UseNpgsql(connectionString));

            return services;
        }
    }
}
