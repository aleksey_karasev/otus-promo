﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Entityframework
{
    public class ApplicationDbContext : DbContext
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Employee>()
                .HasMany(m => m.Roles)
                .WithOne(o => o.Employee)
                .IsRequired();

            modelBuilder.Entity<Employee>().Property(p => p.FirstName).HasMaxLength(50);
            modelBuilder.Entity<Employee>().Property(p => p.LastName).HasMaxLength(50);
            modelBuilder.Entity<Employee>().Property(p => p.Email).HasMaxLength(50);

            modelBuilder.Entity<Role>().Property(p => p.Name).HasMaxLength(50);
        }
    }
}
